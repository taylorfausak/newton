{- |
    Newton is a pure Haskell physics engine.
-}
module Newton where

data Entity = NewEntity
    { position :: Float
    , velocity :: Float
    , acceleration :: Float
    , radius :: Float
    } deriving (Eq, Read, Show)

newEntity :: Entity
newEntity = NewEntity
    { position = 0.0
    , velocity = 0.0
    , acceleration = 0.0
    , radius = 1.0
    }

type Entities = [Entity]

step :: Float -> Entities -> Entities
step time entities = map (stepEntity time entities) entities

stepEntity :: Float -> Entities -> Entity -> Entity
stepEntity time entities entity =
    let newVelocity = velocity entity + acceleration entity * time
        newPosition = position entity + (velocity entity + newVelocity) / 2 * time

        otherEntities = filter (/= entity) entities
        collisions = filter
            (collidingWith entity { position = newPosition })
            otherEntities

        (p', v') = if null collisions
            then (newPosition, newVelocity)
            else (position entity, -newVelocity)

    in  entity { position = p', velocity = v' }

distance :: Entity -> Entity -> Float
distance x y = abs (position x - position y) - (radius x + radius y)

collidingWith :: Entity -> Entity -> Bool
collidingWith x y = distance x y < 0
