module Main where

import qualified Graphics.Gloss.Interface.Pure.Simulate as Gloss
import qualified Newton

type Model = Newton.Entities

main :: IO ()
main = do
    let displayMode = Gloss.InWindow "Newton" (512, 512) (0, 0)
        backgroundColor = Gloss.white
        stepRate = 32
        model =
            [ Newton.newEntity { Newton.radius = 50, Newton.position = 150, Newton.acceleration = -10 }
            , Newton.newEntity { Newton.radius = 50, Newton.velocity = -20 }
            , Newton.newEntity { Newton.radius = 50, Newton.position = -150 }
            ]

    Gloss.simulate
        displayMode
        backgroundColor
        stepRate
        model
        drawModel
        handleStep

drawModel :: Model -> Gloss.Picture
drawModel entities = Gloss.pictures (map drawEntity entities)

drawEntity :: Newton.Entity -> Gloss.Picture
drawEntity entity =
    let r = Newton.radius entity
        y = Newton.position entity
    in  Gloss.translate 0 y (Gloss.circle r)

handleStep :: Gloss.ViewPort -> Float -> Model -> Model
handleStep _viewPort time model = Newton.step time model
