module Main where

import Test.Tasty.Hspec

import qualified Newton
import qualified Test.Tasty as Tasty

main :: IO ()
main = do
    tests <- testSpec "newton" spec
    Tasty.defaultMain tests

spec :: Spec
spec = parallel $ do
    describe "step" $ do
        it "does nothing when there are no entities" $ do
            let entities = []
            Newton.step 1.0 entities `shouldBe` entities

        it "does nothing when no time has passed" $ do
            let entities = [Newton.newEntity { Newton.velocity = 1.0 }]
            Newton.step 0.0 entities `shouldBe` entities

        it "updates the position based on the velocity" $ do
            let oldEntities = [Newton.newEntity { Newton.velocity = 1.0 }]
            let newEntities = [Newton.newEntity { Newton.position = 1.0, Newton.velocity = 1.0 }]
            Newton.step 1.0 oldEntities `shouldBe` newEntities

        it "updates the velocity based on the acceleration" $ do
            let entity = Newton.newEntity { Newton.acceleration = 1.0 }
            Newton.step 1.0 [entity] `shouldBe`
                [entity { Newton.position = 0.5, Newton.velocity = 1.0 }]

        it "detects collisions" $ do
            let oldEntities =
                    [ Newton.newEntity
                    , Newton.newEntity { Newton.position = 2, Newton.velocity = -1 }
                    ]
            let newEntities =
                    [ Newton.newEntity
                    , Newton.newEntity { Newton.position = 2, Newton.velocity = 1 }
                    ]
            Newton.step 1.0 oldEntities `shouldBe` newEntities

    describe "distance" $ do
        it "computes the distance between two entities" $ do
            let x = Newton.newEntity
            let y = Newton.newEntity
            Newton.distance x y `shouldBe` -2

    describe "collidingWith" $ do
        it "is true when entities are colliding" $ do
            let x = Newton.newEntity
            let y = Newton.newEntity
            Newton.collidingWith x y `shouldBe` True

        it "is false when entities are not colliding" $ do
            let x = Newton.newEntity
            let y = Newton.newEntity { Newton.position = 2 }
            Newton.collidingWith x y `shouldBe` False
