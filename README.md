# [Newton][]

[![Build][]](https://gitlab.com/ci/projects/8316?ref=master)

Newton is a pure Haskell physics engine.

[newton]: https://gitlab.com/taylorfausak/newton
[build]: https://gitlab.com/ci/projects/8316/status.svg?ref=master
